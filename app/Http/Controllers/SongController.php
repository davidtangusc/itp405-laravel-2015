<?php namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Validator;
use App\Models\Song;

class SongController extends Controller {

	public function create()
	{
		$artists = DB::table('artists')->orderBy('artist_name', 'asc')->get();
		$genres = DB::table('genres')->get();

		return view('songform', [
			'artists' => $artists,
			'genres' => $genres
		]);
	}


	public function storeSong(Request $request)
	{
		// $validator = Validator::make($input, [
		// 	'title' => 'required',
		// 	'artist_id' => 'required|integer',
		// 	'genre_id' => 'required|integer',
		// 	'price' => 'required|numeric'
		// ]);
		$validator = Song::validate($request->all());

		if ($validator->passes()) {
			// DB::table('songs')->insert($input);
			Song::create([
				'title' => $request->input('title'),
				'artist_id' => $request->input('artist_id'),
				'genre_id' => $request->input('genre_id'),
				'price' => $request->input('price')
			]);

			return redirect('/songs/new')->with('success', 'Song successfully added.');
		}

		return redirect('/songs/new')->withErrors($validator)->withInput();
	}


}