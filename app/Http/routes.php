<?php

Route::get('/', 'WelcomeController@index');
Route::get('home', 'HomeController@index');
Route::get('/songs/new', 'SongController@create');
Route::post('songs', 'SongController@storeSong');