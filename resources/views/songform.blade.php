@extends('layout')

@section('footer')
	some special footer stuff
@stop

@section('content')
	@if (Session::has('success'))
		<p>{{ Session::get('success') }}</p>
	@endif

	@foreach ($errors->all() as $error)
		<p>{{ $error }}</p>
	@endforeach

	<form method="post" action="{{ url("songs") }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		<div class="form-group">
			<label>Title</label>
			<input name="title" class="form-control" value="{{ Request::old('title') }}">
		</div>
		<div class="form-group">
			<label>Artist</label>
			<select class="form-control" name="artist_id">
				@foreach ($artists as $artist)
					<option value="{{ $artist->id }}">{{ $artist->artist_name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Genre</label>
			<select class="form-control" name="genre_id" value="{{ Request::old('genre_id') }}">
				@foreach ($genres as $genre)
					@if ($genre->id == Request::old('genre_id'))
						<option selected="1" value="{{ $genre->id }}">{{ $genre->genre }}</option>
					@else
						<option value="{{ $genre->id }}">{{ $genre->genre }}</option>
					@endif
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Price</label>
			<input name="price" class="form-control" value="{{ Request::old('price') }}">
		</div>
		<button type="submit" class="btn btn-default">Submit</button>
	</form>
@stop


