<!DOCTYPE html>
<html>
<head>
	<title>Add Song</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
</head>
<body>

<?php if (Session::has('success')) : ?>
	<p><?php echo Session::get('success') ?></p>
<?php endif ?>

<?php foreach ($errors->all() as $error) : ?>
	<p><?php echo $error ?></p>
<?php endforeach ?>

<div class="container">
	<form method="post" action="<?php echo url("songs") ?>">
		<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" />
		<div class="form-group">
			<label>Title</label>
			<input name="title" class="form-control" value="<?php echo Request::old('title') ?>">
		</div>
		<div class="form-group">
			<label>Artist</label>
			<select class="form-control" name="artist_id">
				<?php foreach ($artists as $artist) : ?>
					<option value="<?php echo $artist->id ?>"><?php echo $artist->artist_name ?></option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="form-group">
			<label>Genre</label>
			<select class="form-control" name="genre_id" value="<?php echo Request::old('genre_id') ?>">
				<?php foreach ($genres as $genre) : ?>
					<?php if ($genre->id == Request::old('genre_id')) : ?>
						<option selected="1" value="<?php echo $genre->id ?>"><?php echo $genre->genre ?></option>
					<?php else : ?>
						<option value="<?php echo $genre->id ?>"><?php echo $genre->genre ?></option>
					<?php endif ?>
				<?php endforeach ?>
			</select>
		</div>
		<div class="form-group">
			<label>Price</label>
			<input name="price" class="form-control" value="<?php echo Request::old('price') ?>">
		</div>
		<button type="submit" class="btn btn-default">Submit</button>
	</form>
</div>

</body>
</html>